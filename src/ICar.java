/**
 * Different types in java:
 * Class
 * enum
 * Primitive
 * interface
 * 
 * A class is a combination of data and methods.
 * 
 * An interface only defines methods that its subclasses will have
 * By definition, everything in an interface is public
 * 
 * 
 * 
 * @author unouser
 *
 */
public interface ICar 
{
	String getMake();
	String getModel();
	int getYear();
	int getMileage();
}
